package com.ematiq.exchange.services

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import com.ematiq.exchange.services.MasterExchangeAgent.{Convert, SuccessfulResponse}
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}
import scala.concurrent.duration._
import scala.language.postfixOps

class MasterExchangeAgentTest extends AnyWordSpec with BeforeAndAfterAll with Matchers {
  /*
  val testKit = ActorTestKit()
  implicit val system = testKit.system
  override def afterAll(): Unit = testKit.shutdownTestKit()

  val greeting = "Hello there"
  val sender = testKit.spawn(MasterExchangeAgent.getCachedExchangeRate(), "greeter")
  val probe = testKit.createTestProbe[MasterExchangeAgent.RouteResponse]()
  sender ! Convert("EUR", "USD", BigDecimal.valueOf(10), getFormattedDate, probe.ref)
  probe.expectMessage(SuccessfulResponse(BigDecimal.valueOf(8.18073)))

  def getFormattedDate: Date ={
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    format.setTimeZone(TimeZone.getTimeZone("UTC"))
    format.parse("2021-05-18T21:32:42.324Z")
  }
   */
}
