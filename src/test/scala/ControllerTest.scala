import akka.actor.testkit.typed.scaladsl.TestProbe
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.{HttpEntity, HttpMethods, HttpRequest, MediaTypes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.util.ByteString
import com.ematiq.exchange.controller.TradeRoutes
import com.ematiq.exchange.services.MasterExchangeAgent.{Convert, FailedResponse, MasterCommand, SuccessfulResponse}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class ControllerTest extends AnyWordSpec with Matchers with ScalatestRouteTest {
  import akka.actor.typed.scaladsl.adapter._
  implicit val typedSystem: ActorSystem[Nothing] = system.toTyped

  val jsonRequest: ByteString = ByteString(
    s"""
       |{"marketId": 123456, "selectionId": 987654, "odds": 2.2, "stake": 100, "currency": "USD", "date": "2021-05-18T21:32:42.324Z"}
        """.stripMargin)

  val postRequest: HttpRequest = HttpRequest(
    HttpMethods.POST,
    uri = "/api/v1/conversion/trade",
    entity = HttpEntity(MediaTypes.`application/json`, jsonRequest))

  val probe: TestProbe[MasterCommand] = TestProbe[MasterCommand]()

  "Controller" should {
    "return correct stake value and currency when service successfully make conversion" in {
      val expectedResponse = "{\"currency\":\"EUR\",\"date\":\"2021-05-18T21:32:42.324Z\",\"marketId\":123456,\"odds\":2.2,\"selectionId\":987654,\"stake\":150}"

      val test = postRequest ~> new TradeRoutes(probe.ref)(typedSystem).apiRoutes
      val message = probe.expectMessageType[Convert]
      message.replyTo ! SuccessfulResponse(BigDecimal.valueOf(150))
      test ~> check {
        status.isSuccess() shouldEqual true
        responseAs[String] shouldEqual expectedResponse
      }
    }
    "return error code 500 when there is any problem during conversion" in {
      val test = postRequest ~> new TradeRoutes(probe.ref)(typedSystem).apiRoutes
      val message = probe.expectMessageType[Convert]
      message.replyTo ! FailedResponse(new Exception("error"))
      test ~> check {
        status.isSuccess() shouldEqual false
      }
    }
  }
}
