package com.ematiq.exchange

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import com.ematiq.exchange.controller.TradeRoutes
import com.ematiq.exchange.services.MasterExchangeAgent

object Application {
  def main(args: Array[String]): Unit = {
    val rootBehavior = Behaviors.setup[Nothing] { context =>
      implicit val system: ActorSystem[Nothing] = context.system

      val cachedServiceActor = context.spawn(MasterExchangeAgent.getCachedExchangeRate(), "CacheService")
      context.watch(cachedServiceActor)

      val routes = new TradeRoutes(cachedServiceActor)(context.system)

      Http().newServerAt("localhost", 8080).bind(routes.apiRoutes)

      Behaviors.empty
    }
    ActorSystem[Nothing](rootBehavior, "Ematiq")
  }
}
