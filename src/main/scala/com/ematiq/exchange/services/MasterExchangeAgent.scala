package com.ematiq.exchange.services

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.model.DateTime
import com.ematiq.exchange.models.Models.ExchangeMessage
import com.ematiq.exchange.services.ExchangeRateAgent.{ExchangeRateCommand, KillAgent}

import java.text.SimpleDateFormat
import java.util.{Date, UUID}
import scala.concurrent.TimeoutException
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

object MasterExchangeAgent {

  sealed trait MasterCommand

  final case class Convert(sourceCurrency: String, targetCurrency: String, stake: BigDecimal, date: Date, replyTo: ActorRef[RouteResponse]) extends MasterCommand

  final case class ConvertResponseSuccessful(convertedPrice: BigDecimal, messageId: UUID) extends MasterCommand

  final case class ConvertResponseFailed(exception: Throwable, messageId: UUID) extends MasterCommand

  final case class KillChild(childRef: ActorRef[ExchangeRateCommand]) extends MasterCommand

  final case class ClearCache(messageId: UUID) extends MasterCommand

  sealed trait RouteResponse

  final case class SuccessfulResponse(value: BigDecimal) extends RouteResponse

  final case class FailedResponse(exception: Throwable) extends RouteResponse


  def getCachedExchangeRate(apiClients: Map[String, ActorRef[ExchangeRateAgent.Convert]] = Map[String, ActorRef[ExchangeRateAgent.Convert]](),
                            messageMap: Map[UUID, ExchangeMessage] = Map[UUID, ExchangeMessage]()
                           ): Behavior[MasterCommand] =
    Behaviors.setup { context =>
      Behaviors.receiveMessage[MasterCommand] {
        case Convert(sourceCurrency, targetCurrency, stake, date, replyTo) =>
          val formattedDate: String = getFormatedDate(date)
          val mapKey: String = s"${targetCurrency}-${formattedDate}"

          val childBehavior = ExchangeRateAgent.start(targetCurrency, formattedDate, context.self)
          val actorReference: ActorRef[ExchangeRateAgent.Convert] = apiClients.getOrElse(mapKey, context.spawn(childBehavior, mapKey))

          val messageUUID = UUID.randomUUID();
          val message: ExchangeMessage = ExchangeMessage(DateTime.now, replyTo)

          actorReference ! ExchangeRateAgent.Convert(sourceCurrency, stake, messageUUID, context.self)

          context.scheduleOnce(10 second, context.self, ClearCache(messageUUID))

          if (!apiClients.contains(mapKey)) {
            context.log.debug(s"Actor ${mapKey} has been spawned")
            getCachedExchangeRate(apiClients + (mapKey -> actorReference), messageMap + (messageUUID -> message))
          } else {
            getCachedExchangeRate(apiClients, messageMap + (messageUUID -> message))
          }
        case KillChild(childRef) =>
          context.log.debug(s"Removing actor ${childRef.path.name} from cache")
          childRef ! KillAgent()
          getCachedExchangeRate(apiClients - childRef.path.name, messageMap)
        case ConvertResponseSuccessful(convertedPrice, messageId) =>
          val message = messageMap.get(messageId)
          if (message.isEmpty) {
            Behaviors.same
          } else {
            message.get.replyTo ! SuccessfulResponse(convertedPrice)
            getCachedExchangeRate(apiClients, messageMap - messageId)
          }
        case ConvertResponseFailed(exception, messageId) =>
          val message = messageMap.get(messageId)
          if (message.isEmpty) {
            Behaviors.same
          } else {
            message.get.replyTo ! FailedResponse(exception)
            getCachedExchangeRate(apiClients, messageMap - messageId)
          }
        case ClearCache(messageId) =>
          val message = messageMap.get(messageId)
          if (message.isEmpty) {
            Behaviors.same
          } else {
            context.log.debug(s"Message ${messageId} did timeout")
            message.get.replyTo ! FailedResponse(new TimeoutException("Stake has not been exchanged in time."))
            getCachedExchangeRate(apiClients, messageMap - messageId)
          }
      }
    }

  def getFormatedDate(date: Date): String = {
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    formatter.format(date);
  }
}
