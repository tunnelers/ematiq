package com.ematiq.exchange.services

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.{Http, HttpExt}
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.typesafe.config.{Config, ConfigFactory}
import spray.json.DefaultJsonProtocol.jsonFormat1
import spray.json.RootJsonFormat

import scala.concurrent.ExecutionContext.Implicits.global
import spray.json.DefaultJsonProtocol._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import scala.concurrent.Future
import scala.language.postfixOps

class ExchangeRateService(implicit val system: ActorSystem[_]) {

  private val conf: Config = ConfigFactory.load
  private val apiKey: String = conf.getString("fixer-api-key")
  private val http: HttpExt = Http(system)

  def fetchRates(targetCurrency: String, dateString: String): Future[Map[String, BigDecimal]] = {
    try {
      callPrimaryService(targetCurrency, dateString)
        .fallbackTo(callFallbackService(targetCurrency, dateString))
    } catch {
      case e: Throwable => Future.failed(e)
    }
  }

  private def callPrimaryService(targetCurrency: String, dateString: String): Future[Map[String, BigDecimal]] = {
    val params = Map("base" -> s"${targetCurrency}")

    val request: HttpRequest = HttpRequest(
      method = HttpMethods.GET,
      uri = Uri(s"https://api.exchangerate.host/${dateString}").withQuery(Query(params))
    )

    callRemoteService(request)
  }

  private def callFallbackService(targetCurrency: String, dateString: String): Future[Map[String, BigDecimal]] = {
    val params = Map("base" -> targetCurrency,
      "access_key" -> apiKey)

    val request: HttpRequest = HttpRequest(
      method = HttpMethods.GET,
      uri = Uri(s"http://data.fixer.io/api/${dateString}").withQuery(Query(params))
    )
    callRemoteService(request)
  }

  private def callRemoteService(request: HttpRequest): Future[Map[String, BigDecimal]] = {
    http.singleRequest(request)
      .map(response =>
        Unmarshal(response.entity).to[RestEntity]
          .map(rates => rates.rates)
      ).flatMap(fut => fut)
  }

  private case class RestEntity(rates: Map[String, BigDecimal])

  private implicit val restEntityJsonFormat: RootJsonFormat[RestEntity] = jsonFormat1(RestEntity)
}
