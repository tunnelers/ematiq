package com.ematiq.exchange.services

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, StashBuffer}
import com.ematiq.exchange.services.MasterExchangeAgent.{ConvertResponseFailed, ConvertResponseSuccessful, KillChild, MasterCommand}

import java.util.UUID
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.math.BigDecimal.RoundingMode
import scala.util.{Failure, Success}

object ExchangeRateAgent {
  sealed trait ExchangeRateCommand

  final case class Convert(sourceCurrency: String, stake: BigDecimal, messageId: UUID, replyTo: ActorRef[MasterCommand]) extends ExchangeRateCommand

  final case class FetchConversionRates(ratesMap: Map[String, BigDecimal]) extends ExchangeRateCommand

  final case class Error(exception: Throwable) extends ExchangeRateCommand

  final case class KillAgent() extends ExchangeRateCommand

  def start(targetCurrency: String, dateString: String, parent: ActorRef[MasterCommand]): Behavior[ExchangeRateCommand] = {
    Behaviors.withStash(10) { buffer =>
      Behaviors.setup[ExchangeRateCommand] { context =>
        context.scheduleOnce(2 hour, parent, KillChild(context.self))
        new ExchangeRateAgent(targetCurrency, dateString, parent, context, buffer).initializeConversionRates()
      }
    }
  }

  class ExchangeRateAgent(targetCurrency: String,
                          dateString: String,
                          parent: ActorRef[MasterCommand],
                          context: ActorContext[ExchangeRateCommand],
                          buffer: StashBuffer[ExchangeRateCommand]) {
    def initializeConversionRates(): Behavior[ExchangeRateCommand] = {
      context.pipeToSelf(new ExchangeRateService()(context.system).fetchRates(targetCurrency, dateString)) {
        case Success(value) =>
          FetchConversionRates(value)
        case Failure(e) =>
          Error(e)
      }

      Behaviors.receiveMessage {
        case FetchConversionRates(value) =>
          buffer.unstashAll(active(value))
        case Error(e) =>
          //respond 500 to every request, than kill himself
          context.log.error(s"Error during init load of exchange rates, will kill the agent ${e.getMessage}")
          buffer.unstashAll(initializationFailed(e))
          parent ! KillChild(context.self)
          Behaviors.same
        case other =>
          buffer.stash(other)
          Behaviors.same
      }
    }

    private def active(ratesMap: Map[String, BigDecimal]): Behavior[ExchangeRateCommand] = {
      Behaviors.setup { context =>
        Behaviors.receiveMessagePartial {
          case Convert(sourceCurrency, stake, messageId, replyTo) =>
            try {
              val rate: BigDecimal = ratesMap
                .find(currencyRate => currencyRate._1.equals(sourceCurrency))
                .map(currencyRate => currencyRate._2)
                .get

              val convertedStake: BigDecimal = stake / rate
              val result: BigDecimal = convertedStake.setScale(5, RoundingMode.HALF_UP)

              replyTo ! ConvertResponseSuccessful(result, messageId)
            } catch {
              case e: Throwable =>
                replyTo ! ConvertResponseFailed(e, messageId)
            }

            Behaviors.same
          case KillAgent() =>
            context.log.debug(s"Actor ${context.self.path.name} has been killed")
            Behaviors.stopped
        }
      }
    }

    private def initializationFailed(exception: Throwable): Behavior[ExchangeRateCommand] = {
      Behaviors.receiveMessagePartial {
        case Convert(sourceCurrency, stake, messageId, replyTo) =>
          replyTo ! ConvertResponseFailed(exception, messageId)
          Behaviors.same
      }
    }
  }
}
