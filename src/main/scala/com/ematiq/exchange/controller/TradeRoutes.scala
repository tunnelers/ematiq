package com.ematiq.exchange.controller

import akka.actor.typed.scaladsl.AskPattern.Askable
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import com.ematiq.exchange.models.Models.ExchangeRequestModel
import com.ematiq.exchange.models.Models.TradeJsonProtocol._
import com.ematiq.exchange.services.MasterExchangeAgent.{Convert, FailedResponse, SuccessfulResponse}
import com.ematiq.exchange.services.MasterExchangeAgent

import java.util.Date
import scala.concurrent.Future
import akka.actor.typed.scaladsl.AskPattern._

import java.time.Duration

class TradeRoutes(cacheService: ActorRef[MasterExchangeAgent.MasterCommand])(implicit val system: ActorSystem[_]) {

  private implicit val timeout: Timeout = Timeout.create(Duration.ofSeconds(30))

  def convert(sourceCurrency: String, targetCurrency: String, stake: BigDecimal, date: Date): Future[MasterExchangeAgent.RouteResponse] =
    cacheService.ask(Convert(sourceCurrency, targetCurrency, stake, date, _))

  lazy val apiRoutes: Route = path("api" / "v1" / "conversion" / "trade") {
    post {
      entity(as[ExchangeRequestModel]) { request =>

        val targetCurrency = "EUR"

        onSuccess(convert(request.currency, targetCurrency, request.stake, request.date)) {
          case SuccessfulResponse(value) =>
            val response = ExchangeRequestModel(request.marketId,
              request.selectionId,
              request.odds,
              value,
              targetCurrency,
              request.date)
            complete(StatusCodes.OK, response)
          case FailedResponse(e) =>
            system.log.error(e.getMessage)
            complete(StatusCodes.InternalServerError)
        }
      }
    }
  }
}
