package com.ematiq.exchange.models

import akka.actor.typed.ActorRef
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.DateTime
import com.ematiq.exchange.services.MasterExchangeAgent.RouteResponse
import spray.json.{DefaultJsonProtocol, JsString, JsValue, JsonFormat, RootJsonFormat, deserializationError}

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}
import scala.util.Try

object Models {
  case class ExchangeMessage(sendTime: DateTime, replyTo: ActorRef[RouteResponse])

  case class ExchangeRequestModel(marketId: Long, selectionId: Long, odds: BigDecimal, stake: BigDecimal, currency: String, date: Date)

  object TradeJsonProtocol extends SprayJsonSupport with DefaultJsonProtocol {

    implicit object DateFormat extends JsonFormat[Date] {
      def write(date: Date): JsString = JsString(dateToIsoString(date))

      def read(json: JsValue): Date = json match {
        case JsString(rawDate) =>
          parseIsoDateString(rawDate)
            .fold(deserializationError(s"Date should be in format yyyy-MM-dd'T'HH:mm:ss.SSSXXX, got $rawDate"))(identity)
        case error => deserializationError(s"Expected JsString, got $error")
      }
    }

    private val localIsoDateFormatter = new ThreadLocal[SimpleDateFormat] {
      override def initialValue() = {
        val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        format.setTimeZone(TimeZone.getTimeZone("UTC"))
        format
      }
    }

    private def dateToIsoString(date: Date) =
      localIsoDateFormatter.get().format(date)

    private def parseIsoDateString(date: String): Option[Date] =
      Try {
        localIsoDateFormatter.get().parse(date)
      }.toOption

    implicit val addressFormat: RootJsonFormat[ExchangeRequestModel] = jsonFormat6(ExchangeRequestModel.apply)
  }
}
